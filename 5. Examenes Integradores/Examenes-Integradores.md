# **Examenes Integradores**

## **Primer examen**

- **Las TIC sólo se pueden implementar en ámbitos dedicados a la tecnología**

  - Verdadero
  - Falso ✔

- **¿Para qué sirven las TIC?**

  - Para brindar servicios online, como un homebanking
  - Para brindar servicios de comunicación eficaces y eficientes
  - Para agilizar la manera de hacer negocios
  - Todas son correctas ✔

- **Si "a" es falso, "no a" es:**

  - Verdadero ✔
  - Falso

- **No se necesita planificar para diseñar un algoritmo**

  - Verdadero
  - Falso ✔

- **¿Cuál es la sentencia SQL para eliminar una base de datos?**

  - DELETE DATABASE nombre_db
  - REMOVE DATABASE nombre_db
  - DROP DATABASE nombre_db ✔
  - Ninguna respuesta es correcta

- **En las bases de datos relacionales las tablas sólo pueden relacionarse entre sí de una única manera**

  - Verdadero
  - Falso ✔

- **Si necesito que mi rango de datos cambie de formato horizontal a vertical:**

  - Utilizo la función TRANSPONER(rango)
  - Utilizo pegado especial transpuesto
  - Las dos opciones son correctas ✔
  - Ninguna opción es correcta

- **La función BUSCARH busca un valor en:**

  - Cualquier fila del rango y devuelve todos los datos de la misma columna
  - La fila superior del rango y devuelve todos los datos de la misma columna
  - Cualquier fila del rango y devuelve solamente un dato de la misma columna
  - La fila superior del rango y devuelve solamente un dato de la misma columna ✔
  - Cualquier fila del rango y devuelve solamente un dato de la misma columna

- **El KPI**

  - Es el objetivo a cumplir
  - Es una métrica de performance ✔
  - La dimensión clave a evaluar
  - Ninguna respuesta es correcta

**Cambie el valor de "apple" por "kiwi", en la lista de frutas:**

```py
fruits = ["apple", "banana", "cherry"]
```

- Seleccione una:

  - fruits[1]="kiwi"
  - fruits[1]="kiwi";
  - fruits[0]="kiwi" ✔

- **Inserte "lemon" en la segunda posición en la lista de frutas:**

```py
fruits = ["apple", "banana", "cherry"]
```

- Seleccione una:

  - fruits.insert(1,"lemon") ✔
  - fruits.insert(2,"lemon")
  - fruits[2]="lemon"

- **¿Qué imprime en consola el siguiente código?**

```py
lista = [1,2,3]
copia = lista
lista.pop()
print(|copia)
```

- Seleccione una:

  - [1, 2] ✔
  - [1, 2, 3]
  - TypeError

- **¿Qué imprime en consola el siguiente código?**

```py
def saludar(nombre, signo = "!"):
    print(f'Hola {nombre}{signo}')

saludar("alumnos", " ")
```

- Seleccione una:

  - f'Hola {nombre}{signo}'
  - Hola alumnos ✔
  - Hola alumnos!

- **Entre los datos con la edad de un grupo de personas, se ha encontrado el primer cuartil en 27 años. Puedo afirmar que el 25% de la muestra tiene como máximo 27 años**

  - Verdadero ✔
  - Falso

- **En todo conjunto de datos numéricos homogéneos se puede encontrar media, moda y mediana**

  - Verdadero
  - Falso ✔

- **df1.values devuelve los valores de una serie en forma de**

  - Cadena de texto
  - Lista Respuesta correcta
  - Diccionario
  - Tupla

- **Sobre dos series en pandas podemos ejecutar:**

  - suma
  - resta
  - multiplicación
  - Todas las respuestas son correctas ✔

- **¿Qué ploteo nos da un resumen estadístico con vista de los cuartiles?**

  - Caja y bigotes Respuesta correcta ✔
  - Líneas
  - Histograma
  - Barras

- **¿Cuál de las siguientes afirmaciones es incorrecta?**

  - La visualización de datos es uno de los recursos más importantes en análisis de datos
  - Visualizar grandes volúmenes de datos complejos no es efectivo ✔
  - Podemos visualizar datos utilizando la librería Motplotlib en python
  - Los tomadores de decisiones utilizan la visualización de datos para comprender las problemáticas del negocio más fácilmente y planificar estrategias

- **¿Cuál es la salida del siguiente código?**

```py
import numpy as np

arr = np.array([1,2,3,5,8])
arr = arr + 1
print(arr[1])
```

- Seleccione una:
  - 0
  - 1
  - 2
  - 3 ✔

---

## **Segundo examen**

- **Un profesional de Big Data puede llevar adelante cualquier proyecto completo**

  - Verdadero
  - Falso ✔

- **¿Cómo se llama el protocolo que utiliza nuestra PC para conectarse a Internet?**

  - HTTP
  - Hosting
  - TCP/IP ✔
  - URL

- **"a" es verdadero y "b" es falso. "a o b" es:**

  - Verdadero ✔
  - Falso

- **Si "a" es verdadero, "no a" es:**

  - Verdadero
  - Falso ✔

- **Estoy buscando en mi base de datos a los alumnos entre 8 y 12 años. La sentencia SQL que necesito es:**

  - SELECT \* FROM alumnos WHERE edad >= 8 AND edad <=12;
  - SELECT \* FROM alumnos WHERE edad BETWEEN 8 AND12;
  - Ambas son correctas ✔

- **¿Qué es la normalización de bases de datos?**

  - Una función especial de las planillas de cálculo para las bases de datos
  - Concentrar todos los datos en una única tabla
  - Distribuir los datos en tablas relacionadas ✔
  - Todas las respuestas son correctas

- **¿Para qué sirve la función CONTAR.SI.CONJUNTO?**

  - Cuenta la cantidad de datos dentro de un rango que cumplen dos o más condiciones ✔
  - Devuelve la suma de un rango de datos si se cumple una condición
  - Cuenta la cantidad de datos dentro de un rango que cumplen una condición
  - Devuelve la suma de un conjunto de datos

- **El formato .csv permite agregar estilo a sus celdas**

  - Verdadero
  - Falso ✔

- **Cantidad de visitas, compras concretadas, likes son:**

  - Dimensiones
  - Métricas ✔

- **Elija una opción para agregar "orange" a la lista de frutas:**

```py
fruits = ["apple", "banana", "cherry"]
```

- Seleccione una:

  - fruits[4]="orange"
  - fruits.add="orange"
  - fruits.append("orange") ✔

- **¿Cuál de las siguientes no es una estructura de datos en python?**

  - dict
  - list
  - Class ✔
  - tuple

- **Indique la opción correcta para obtener el último elemento de la lista de frutas**

```py
fruits = ["apple", "banana", "cherry"]
```

- Seleccione una:

  - print(fruits[3])
  - print(fruits[len(fruits)])
  - print(fruits[-1]) ✔

- **¿Cuál es la salida de la siguiente línea?**

```py
print(9 // 2)
```

- Seleccione una:

  - 4 ✔
  - 4.0
  - 4.5
  - Error

- **La matriz de covarianza permite:**

  - Agilizar la reducción de datos
  - Analizar las correlaciones de varias combinaciones de pares de variables de una vez
  - Ver rápidamente qué variables dan información redundante
  - Todas las respuestas son correctas ✔
  - Ninguna respuesta es correcta

- **En una distribución normal**

  - Los "outliers" están más allá de 3 desviaciones standard de la media
  - La curva es simétrica respecto de la media
  - El 68,27% de los datos se encuentra a 1 desviación estándar de la media
  - Todas las respuestas son correctas ✔
  - Ninguna respuesta es correcta

- **Podemos asignar etiquetas personalizadas al índice de una serie**

  - Verdadero ✔
  - Falso

- **¿Cómo se instala numpy en una computadora con Windows y Python 3?**

  - pip3 install numpy
  - pip install numpy ✔
  - python3 install numpy
  - Ninguna respuesta es correcta

- **¿Cuál de las siguientes afirmaciones sobre Pandas es correcta?**

  - Las columnas pueden contener diferentes tipos de datos
  - Puede efectuar operaciones matemáticas en filas y columnas
  - Tiene ejes etiquetados (en filas y columnas)
  - Todas las respuestas son correctas ✔

- **¿Cuál es la salida del siguiente código?**

```py
fetch numpy as np
arr1 = np.array([0,1,2,3,5,8])
print(arr1)
```

- Seleccione una:

  - [0 1 2 3 5 8]
  - SyntaxError ✔

- **¿Para qué sirve array.min()?**

  - Encuentra el mayor valor en un array de numpy
  - Encuentra el menor valor en un array de numpy ✔
  - Efectúa una operación de resta
  - Ninguna respuesta es correcta
