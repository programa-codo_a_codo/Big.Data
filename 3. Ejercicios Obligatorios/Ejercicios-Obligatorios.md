# **Preguntas y Respuestas - Ejercicios Obligatorios**

# **Módulo I**

## **Conceptos generales**

- **¿Para qué sirven las TIC?**

  - Para brindar servicios online, como el homebanking
  - Para brindar servicios de comunicación eficaces y eficientes
  - Para agilizar la manera de hacer negocios
  - Todas son correctas ✔

- **Las TIC sólo se pueden implementar en ámbitos dedicados a la tecnología**

  - Verdadero
  - Falso ✔

- **Las TIC son responsables de brindar comunicaciones inmediatas y eficaces**

  - Verdadero ✔
  - Falso

- **Las TIC son totalmente beneficiosas en ámbitos laborales**

  - Verdadero
  - Falso ✔

- **Son TIC:**

  - Redes, Internet, plataformas streaming
  - Versión on-line de un medio gráfico, servicio de homebanking, tablet
  - Blogs, almacenamiento en la nube, smartTV
  - Todas las anteriores ✔

- **Un profesional de Big Data puede llevar adelante cualquier proyecto completo**

  - Verdadero
  - Falso ✔

- **¿Qué es un dominio?**

  - La capacidad de tener control sobre una situación
  - El servidor donde se aloja una página web
  - Una manera de "traducir" la dirección IP ✔
  - Ninguna respuesta es correcta

- **¿Cómo se llama el protocolo que utiliza nuestra PC para conectarse a Internet?**

  - HTTP
  - hosting
  - TCP/IP ✔
  - URL

- **"a" es verdadero y "b" es falso. "a o b" es:**

  - Verdadero ✔
  - Falso

- **"a" es verdadero y "b" es falso. "a y b" es:**

  - Verdadero
  - Falso ✔

- **¿Qué es GDMA?**

  - Una guía de procedimientos para procesar Big Data
  - Una organización que reúne asociaciones de marketing de todo el mundo ✔
  - Una guía de reglas de ética para el tratamiento de Big Data

- **Deep Learning**

  - Es parte del proceso de Machine Learning
  - No tiene relación con Machine Learning
  - Es una tecnología de Machine Learning y de IA ✔
  - Ninguna opción es correcta

- **El proceso de Data Science puede iniciarse en cualesquiera de sus etapas**
  - Verdadero
  - Falso ✔

---

## **Algoritmos**

- **¿Cuál NO es un tipo de algoritmo?**

  - Discreto ✔
  - No computacional
  - Cualitativo
  - Todos son tipos de algoritmos

- **¿Qué es un algoritmo?**

  - Una secuencia de pasos lógicos
  - Un conjunto de reglas ordenadas y finitas
  - Una serie de instrucciones para realizar una tarea
  - Todas son correctas ✔

- **Sólo hay una manera de resolver un problema con un algoritmo**

  - Verdadero
  - Falso ✔

- **¿Qué partes constituyen un algoritmo?**

  - Una estructura de control
  - Un diagrama de flujo
  - Todas son correctas
  - Datos de entrada, un proceso y datos de salida ✔

- **No se necesita planificar para diseñar un algoritmo**

  - Verdadero
  - Falso ✔

---

## **Bases de datos I**

- **Primary key es:**

  - Ninguna es correcta
  - Una columna especial de una tabla ✔
  - Una contraseña específica
  - Una sentencia SQL

- **¿Qué es la normalización de bases de datos?**

  - Una función especial de Excel para las bases de datos
  - Distribuir los datos en tablas relacionadas ✔
  - Concentrar todos los datos en una única tabla

- **¿Cuáles son las sentencias DDL?**

  - UPDATE, ALTER, CREATE
  - Ninguna es correcta
  - CREATE, ALTER, DROP ✔
  - CREATE, INSERT, DELETE

- **En las bases de datos relacionales existe un único tipo de relación entre tablas**

  - Verdadero
  - Falso ✔

- **¿Cuál es la sentencia SQL para eliminar una base de datos?**

  - REMOVE DATABASE nombre_db
  - Ninguna es correcta
  - DELETE DATABASE nombre_db
  - DROP DATABASE nombre_db ✔

---

## **Bases de datos II**

- **La instrucción INSERT permite agregar registros en la base de datos sólo de a uno por vez**

  - Verdadero
  - Falso ✔

- **La sentencia 'DELETE FROM nombre_tabla', elimina la tabla completa**

  - Verdadero
  - Falso ✔

- **Si necesito eliminar a 'Pedro Álvarez' de la tabla 'clientes' la sentencia correcta es:**

  - DELETE FROM clientes;
  - DELETE FROM clientes; WHERE nombre = 'Pedro Álvarez'; ✔
  - DROP FROM clientes; IF nombre = 'Pedro Álvarez';
  - DELETE FROM clientes; IF nombre = 'Pedro Álvarez';

- **Estoy buscando en mi base de datos a los alumnos entre 8 y 12 años. La sentencia SQL que necesito es:**

  - SELECT \* FROM alumnos WHERE edad BETWEEN 8 AND 12;
  - SELECT \* FROM alumnos WHERE edad >= 8 AND edad <= 12;
  - Ambas son correctas ✔

- **¿Qué es un alias?**

  - El cambio de nombre de tablas o columnas cuando hay dos tablas con el mismo nombre de columna
  - Una manera de renombrar de forma temporaria a una tabla o una columna ✔
  - Una sentencia SQL
  - Ninguna es correcta

---

## **Business Intelligence**

- **¿Qué significa ETL?**

  - Extract, Transform y Load ✔
  - Extend, Translate y Load
  - Extract, Translate y Load

- **En una empresa tradicional, ¿Cuál es el orden por importancia más común de stakeholders?**

  - Accionistas, clientes y empleados ✔
  - Empleados, accionistas y clientes
  - Clientes, accionistas y empleados

- **¿Cuál es la razón primordial del Business Intelligence?**

  - Tomar mejores decisiones para satisfacer mejor a los clientes
  - Salvaguardar los intereses de los stakeholders ✔
  - Aprender a comunicar lo que los datos nos informan

- **¿Cuáles son los tipos de costo que podemos encontrar en una empresa?**

  - Fijos, variables y semivariables ✔
  - Se tipifican según el área en que se aplican
  - Brutos, netos y operativos

- **La nómina es un ejemplo de costo....**

  - Fijo ✔
  - Variable
  - Semivariable

- **¿Qué es el margen de contribución?**

  - La diferencia entre las utilidades y los impuestos
  - La relación entre los gastos por servicios y los impuestos
  - La diferencia entre el costo variable unitario y el precio de un producto ✔

- **¿Para qué nos pueden servir las razones matemáticas en los negocios?**

  - Para mejorar la toma de decisiones
  - Para obtener cálculos más precisos
  - Para poder comparar fácilmente ✔

- **¿Qué son los silos de información?**

  - Cúmulos de información aislados ✔
  - Los diferentes fuentes de donde obtener los datos
  - Espacios de almacenamiento de datos masivos

- **¿Qué es el garbage in garbage out?**

  - Una de las partes que compone el proceso de ETL encargado de limpiar los datos de errores
  - El lugar destinado a los datos que no nos resultan útiles
  - La calidad de información que utilizamos al inicio de nuestro proceso. Si metes basura, sale basura ✔

- **Es común cambiar de opinión sobre nuestras suposiciones al realizar exploraciones. Esto es:**

  - Verdadero ✔
  - Falso

- **La exploración es la parte que más tiempo utiliza en el proceso del Business Intelligence**

  - Verdadero ✔
  - Falso

- **Podemos saber mucho sobre la empresa simplemente leyendo el estado de resultados. Esto es:**

  - Verdadero ✔
  - Falso

- **Si pudieras realizar acciones que aumenten un tipo de utilidad, ¿Cuál utilidad preferirías aumentar?**

  - Utilidad neta ✔
  - Utilidad operativa
  - Utilidad bruta

- **Si tu proveedor de materia prima decide aumentar el precio de los materiales con los que fabricas tus productos, ¿Qué utilidad se vería afectada? Considera que todo lo demás permanece igual**

  - Utilidad operativa
  - Utilidad neta
  - Utilidad bruta ✔
  - Todas las respuestas son correctas

- **Si te encontraras con varios silos de información en la empresa, ¿Qué deberías hacer?**

  - Al momento de realizar extracciones, revisar qué silos podrían contener información relevante para el análisis e incorporarlo ✔
  - Integrar todos los silos, unificando formatos, y luego comenzar con el proceso de extracción
  - Buscar otras fuentes de datos alternativas

- **Te piden una presentación de reporte sobre el rendimiento del departamento comercial. Tendrás dos presentaciones, la primera con la jefa del departamento comercial y la segunda con la mesa directiva de la empresa, ¿Qué deberías hacer?**

  - Construir un reporte lo más completo y con la mayor cantidad de información posible, para poder responder las preguntas de cualquier tipo de audiencia
  - Construir un reporte a la medida de cada audiencia, o sea, un reporte para la jefa de departamento y un reporte para la mesa directiva ✔
  - Las dos respuestas son correctas
  - Ninguna respuesta es correcta

---

## **Planilla de cálculos**

- **La función BUSCARV busca un valor en:**

  - Cualquier columna del rango y devuelve todos los datos de esa misma fila
  - Cualquier columna del rango y devuelve solamente un dato de esa misma fila
  - La primera columna de la izquierda del rango y devuelve todos los datos de esa misma fila
  - La primera columna de la izquierda del rango y devuelve solamente un dato de esa misma fila ✔

- **Necesito sumar datos de una columna o fila, pero sólo los que cumplen tres condiciones dadas**

  - Existe una función que lo permite ✔
  - No se puede hacer

- **La función BUSCARH busca un valor en:**

  - Cualquier fila del rango y devuelve solamente un dato de la misma columna
  - La fila superior del rango y devuelve todos los datos de la misma columna
  - La fila superior del rango y devuelve solamente un dato de la misma columna ✔
  - Cualquier fila del rango y devuelve todos los datos de la misma columna

- **Si necesito que mi rango de datos cambie de formato horizontal a vertical:**

  - Ninguna opción es correcta
  - Utilizo la función TRANSPONER(rango)
  - Utilizo pegado especial transpuesto
  - Las dos opciones son correctas ✔

- **¿Para qué sirve la función CONTAR.SI.CONJUNTO?**

  - Devuelve la suma de un rango de datos si se cumple una condición
  - Cuenta la cantidad de datos dentro de un rango que cumplen una condición
  - Cuenta la cantidad de datos dentro de un rango que cumplen dos o más condiciones ✔
  - Devuelve la suma de un conjunto de datos

- **Google Hojas de Cálculo ofrece opciones de trabajo mucho más limitadas que las que ofrecen Excel y otras aplicaciones de escritorio**

  - Verdadero
  - Falso ✔

- **El formato .csv permite agregar estilo a sus celdas**

  - Verdadero
  - Falso ✔

---

## **Looker Studio**

- **Looker Studio es una herramienta gratuita de Google para todo uso**

  - Verdadero
  - Falso ✔

- **Localización, sexo, dispositivo desde el que se conecta son:**

  - Métricas
  - Dimensiones ✔

- **El KPI:**

  - Ninguna respuesta es correcta
  - La dimensión clave a evaluar
  - Es el objetivo a cumplir
  - Una métrica de performance ✔

- **Cantidad de visitas, compras concretadas, likes son:**

  - Dimensiones
  - Métricas ✔

- **Looker Studio es una herramienta apta para equipos de trabajo**

  - Verdadero ✔
  - Falso

- **Cada interacción del usuario con un sitio web genera una nueva sesión**

  - Verdadero
  - Falso ✔

---

# **Módulo II**

---

## **Bases de programación en Python I**

- **Son tipos de datos en Python:**

  - Ninguna es correcta
  - integer, str, float
  - int, varchar, list
  - int, str, float ✔

- **Los tipos de datos en Python se pueden clasificar en mutables e inmutables**

  - Verdadero ✔
  - Falso

- **Python viene preinstalado en el sistema operativo Windows**

  - Verdadero
  - Falso ✔

- **Si instalamos Python, ya podemos ejecutar instrucciones sin instalar software adicional**

  - Verdadero ✔
  - Falso

- **Python no admite tipos de datos compuestos, sólo de elementos individuales**

  - Verdadero
  - Falso ✔

- **¿Cuál estructura de control sería mejor para este caso?**

```py
numero = input("Ingrese su numero")

# Programa para incrementar un número hasta llegar al 10
# Se ingresa por teclado el 7 y la pantalla muestra:

El número 7 es menor que 10
El número 8 es menor que 10
El número 9 es menor que 10
El número 10 es mayor o igual que 10
```

- Seleccione una:

  - for
  - elif
  - if
  - while ✔

- **¿Cuál estructura de control sería mejor para este caso?**

```py
numero = input("Ingrese su numero")

# Programa para ir contando hasta el 10
# Se muestra por pantalla lo siguiente

6
7
8
9
10
```

- Seleccione una:

  - elif
  - if
  - for ✔
  - while

- **¿Cuál estructura de control sería mejor para este caso?**

```py
numero = input("Ingrese su numero")

# Programa para verificar que le número es menor o igual a 10
# Se ingresa el 10 y muestra por pantalla lo siguiente:

El numero: 10 es mayor o igual a 10
```

- Seleccione una:

  - for
  - if ✔
  - elif
  - while

- **¿Cuál estructura de control sería mejor para este caso?**

```py
numero = input("Ingrese su numero")

# Programa para verificar si un número es negativo, positivo menor a 10 o positivo mayor o igual que 10
# Se ingresa el 6 y se muestra por pantalla lo siguiente.

El número 6 es positivo menor a 10.
```

- Seleccione una:

  - for
  - if
  - elif ✔
  - while

- **¿Cuál estructura de control sería mejor para este caso?**

```py
numero = input("Ingrese su numero")

# Programa para verificar si un número es par o impar
# Se ingresa el 7 y la pantalla muestra:

El número 7 es impar.
```

- Seleccione una:

  - for
  - while
  - if ✔
  - elif

---

## **Bases de programación en Python II**

- **El método add() se puede aplicar a un frozenset**

  - Verdadero
  - Falso ✔

- **En una tupla puedo aplicar:**

  - count(), index(), sort()
  - count(), index() ✔
  - count(), index(), insert(), pop()

- **Colección ordenada de elementos significa:**

  - Que se puede acceder a los elementos por medio de un índice ✔
  - Que los elementos están ordenados de menor a mayor
  - Que se puede acceder a los elementos por medio de una clave
  - Que los elementos están ordenados de mayor a menor

- **Una lista puede ser ordenada, invertida, recortada y modificada por variedad de métodos**

  - Verdadero ✔
  - Falso

- **Una tupla puede ser ordenada, invertida, recortada y modificada por variedad de métodos**

  - Verdadero
  - Falso ✔

- **Las listas y las tuplas deben contener solamente datos del mismo tipo**

  - Verdadero
  - Falso ✔

- **Los tipos set y frozenset comparten:**

  - todos los métodos
  - algunos métodos ✔
  - ningún método
  - ninguna respuesta es correcta

- **Las f-strings**

  - Son una forma moderna de formatear cadenas de texto
  - Todas son correctas ✔
  - Permiten incrustar valores de variables dentro de una cadena de texto
  - Admiten expresiones y condicionales

- **Paso una tupla como argumento de una función donde se aplican operaciones sobre ella. Al salir de la función y retornar al programa principal:**

  - La tupla original permanece intacta ✔
  - Tengo un objeto tupla diferente, con las modificaciones que recibí de la función
  - Tengo el mismo objeto tupla con las modificaciones que le aplicó la función

- **datetime es un tipo de dato en Python**

  - Verdadero
  - Falso ✔

- **Para obtener un número aleatorio float entre 0 y 1 utilizamos:**

  - random.random() ✔
  - random()
  - random.float

- **Si necesito una serie de datos que se va a modificar dinámicamente, elijo:**

  - Una lista ✔
  - Una tupla

---

## **POO en Python**

- **¿Cuál es la descripción que define mejor el concepto 'clase' en la programación orientada a objetos?**

  - Es una categoría de datos ordenada secuencialmente
  - Es un concepto similar al de 'array'
  - Es un modelo o plantilla a partir de la cual creamos objetos ✔
  - Es un tipo particular de variable

- **¿Qué elementos definen a un objeto?**

  - Sus atributos y sus métodos ✔
  - La forma en que establecen comunicación e intercambia mensajes
  - Su interfaz y los eventos asociados
  - Su cardinalidad y su tipo

- **¿Qué código de los siguientes tiene que ver con la herencia?**

  - Componente extends Producto
  - Componente(Producto)
  - class Componente belogns Producto
  - class Componente(Producto) ✔

- **¿Qué significa instanciar una clase?**

  - Crear un objeto a partir de la clase ✔
  - Eliminar una clase
  - Conectar dos clases entre sí
  - Duplicar una clase

- **¿Qué es herencia?**

  - Es un mecanismo que permite basar una clase nueva en la definición de una clase existente ✔
  - Es una abstracción de un objeto
  - Es a compilación de varios objetos
  - Es la dependencia de un estado

- **¿Cuál es la función del método constructor?**

  - Para asignar valores a los atributos del objeto ✔
  - Para inicializar la clase
  - Construir un objeto
  - Construir la clase

---

## **BBDD en Python I**

- **Quiero trabajar con la librería tkinter. Nombro mi archivo tkinter.py y:**

  - Se ejecuta normalmente
  - Da error ✔
  - Importa la librería antes de ejecutar el código

- **Las GUI:**

  - Son lo primero que ve el usuario
  - Están compuestas de ventanas y widgets
  - Proporciona un entorno gráfico a los programas
  - Todas las respuestas son correctas ✔

- **Para trabajar una base de datos con sqlite3 debo empezar por:**

  - Crear la base de datos, crear la conexión y ejecutar una instrucción SQL
  - Ninguna opción es correcta
  - Importar el módulo, crear la base de datos y ejecutar una instrucción SQL
  - Importar el módulo, crear la conexión y crear el cursor ✔

- **Para crear una ventana con tkinter:**

  - Ninguna opción es correcta
  - Invoco al método root()
  - Instancio un objeto de la clase Tk ✔
  - Invoco al método root

- **SQLite**

  - Ninguna opción es correcta
  - Necesita servicios de un servidor para acceder a los datos
  - Utiliza almacenamiento en disco local ✔
  - Permite almacenar datos en un servidor remoto

---

## **BBDD en Python II**

- **Para habilitar / deshabilitar un widget en tkinter, utilizamos:**

  - state = ENABLED/DISABLED
  - enabled = TRUE/FALSE
  - state = TRUE/FALSE
  - state = NORMAL/DISABLED ✔

- **El widget para crear un menú desplegable es:**

  - Select
  - MenuSelector
  - OptionMenu ✔
  - OptionSelector

- **La opción 'relief' se refiere a:**

  - Lo que ocurre después de hacer click en un widget
  - Lo que ocurre cuando un widget sale de foco
  - El borde de un widget ✔
  - Ninguna opción es correcta

- **Para abrir una ventana para mostrar información adicional usamos:**

  - messagebox ✔
  - MessageBox
  - showinfo
  - ShowInfo

- **El widget para recibir información ingresada por el usuario es:**

  - mainloop()
  - Input
  - Entry ✔
  - Insert
  - Enter

---

## **Estadistica**

- **Un coeficiente de correlación alto entre dos variables indica que una variable:**

  - Disminuye a la misma tasa que la otra va aumentando
  - Aumenta a la misma tasa que la otra variable ✔
  - Disminuye porque aumenta la otra variable
  - Aumenta porque aumenta la otra variable

- **Los cuartiles son los 3 valores que dividen un conjunto de datos en 4 partes iguales y el cuartil 2 coincide con la mediana**

  - Verdadero ✔
  - Falso

- **Son datos continuos:**

  - Temperatura registrada cada media hora, altura de 10 alumnos, longitud de 100 pernos ✔
  - Días trabajados por 5 empleados, cantidad de acciones vendidas en el día, temperaturas registradas durante 1 semana
  - Temperatura registrada cada media hora, unidades fabricadas, longitud de 100 pernos
  - Ninguna combinación contiene sólo datos continuos

- **En todo conjunto de datos numéricos homogéneos se puede encontrar media, moda y mediana**

  - Verdadero
  - Falso ✔

- **La matriz de covarianza permite:**

  - Agilizar la reducción de datos
  - Analizar las correcciones de varias combinaciones de pares de variables de una vez
  - Ver rápidamente qué variables dan información redundante
  - Todas las respuestas son correctas ✔
  - Ninguna respuesta es correcta

- **La varianza es la raíz cuadrada de la desviación standard**

  - Verdadero
  - Falso ✔

- **Entre los datos con la altura de un grupo de personas, se ha encontrado el tercer cuatril en 172 c- Puedo afirmar que el 75% de la muestra mide como máximo 1,72 m.**

  - Verdadero ✔
  - Falso

- **Son datos discretos:**

  - Cantidad de horas de trabajo semanal, cantidad de acciones vendidas en el día, temperaturas registradas durante 1 semana
  - Cantidad de alumnos aprobados, la escala de un termómetro de mercurio, cantidad de acciones vendidas en el día ✔
  - Temperatura registrada cada media hora, altura de 10 alumnos, longitud de 100 pernos
  - Ninguna combinación contiene sólo datos continuos

- **En una distribución normal:**

  - Los ouliers están más allá de 3 desviaciones standard de la media
  - La curva es simétrica respecto de la media
  - El 68,27% de los datos se encuentra a 1 desviación standard de la media
  - Todas las respuestas son correctas ✔
  - Ninguna respuesta es correcta

- **La mediana se ve afectada por los outliers**

  - Verdadero
  - Falso ✔

- **La varianza es el promedio de los cuadrados de las distancias entre todos los puntos y la media**

  - Verdadero ✔
  - Falso

---

## **Ejercicios complementarios**

- **El comando para eliminar una tabla de una base de datos es:**
	
	- DELETE TABLE
	- TRUNCATE TABLE
	- DROP TABLE ✔
	- Todas las opciones son erróneas

- **La función BUSCARV busca un valor en:**
	
	- Cualquier columna del rango y devuelve todos los datos de esa misma fila
	- Cualquier columna del rango y devuelve solamente un dato de esa misma fila
	- La primera columna de la izquierda del rango y devuelve todos los datos de esa misma fila
	- La primera columna de la izquierda del rango y devuelve solamente un dato de esa misma fila ✔

- **No existe una única forma de resolver un problema mediante algoritmos. Puede haber tantas soluciones diferentes como personas dedicadas a encontrarlas**
	
	- Verdadero ✔
	- Falso

- **Debemos modificar el campo 'activo' de la tabla 'clientes' al valor 'True' cuando el campo 'provincia' indica que el cliente tiene sucursal en la provincia de Córdoba**

	- UPDATE clientes SET activo = TRUE WHERE provincia = 'Córdoba' ✔
	- UPDATE FROM clientes FIELD(activo) value (TRUE)
	- UPLOAD FROM clientes FIELD(activo) value (TRUE)
	- UPDATE FOR clientes FIELD(activo) value (TRUE)
	- Ninguno de los comando es válido

- **¿Cómo puedo compartir mis reportes en Google Data Studio?**

	- Todas las respuestas son correctas ✔
	- Incrustándolo en una página web
	- Otorgando permisos y compartiendo el link
	- Compartiéndolo en PDF

- **Negación, disyunción y conjunción son tres maneras de operar para obtener valores de verdad a partir de una o más proposiciones**

	- Verdadero ✔
	- Falso

- **Para controlar el aspecto de una celda según su contenido/expresión lógica utilizamos:**

	- Formatos condicionales ✔
	- Funciones condicionales
	- Las dos anteriores
	- Ninguna opcion es correcta

- **Con cuales de los siguientes conceptos esta relacionada una clave primaria de SQL:**

	- Un indice de un campo de la tabla con datos no repetibles y puede ser autoincremental ✔
	- Todas las opciones con erróneas
	- Un indice de campos que no puede utilizarse para generar relaciones con otras tablas
	- Un indice de campos que pueden repetirse pero son posibilidad de ser eliminados

- **Las macros permiten ejecutar repetir tareas rutinarias y crear nuevas funciones en nuestras plantillas de cálculo**

	- Verdadero ✔
	- Falso

- **En Google Data Studio puedo definir mis propias métricas calculadas a partir de otras:**

	- Verdadero ✔
	- Falso

- **Paso una tupla como argumento de una función donde se aplican operaciones sobre ella. Al salir de la función y retornar al programa principal:**

	- La tupla original permanece intacta ✔
	- Tengo un objeto tupla diferente, con las modificaciones que recibí de la función
	- Tengo el mismo objeto tupla con las modificaciones que le aplicó la función

- **Si necesito una serie de datos que se va a modificar dinámicamente, elijo:**

	- Una tupla
	- Una lista ✔

- **¿Cuál es la descripción que define mejor el concepto 'clase' en la programación orientada a objetos?**

	- Es una categoría de datos ordenada secuencialmente
	- Es un concepto similar al de 'array'
	- Es un modelo o plantilla a partir de la cual creamos objetos ✔
	- Es un tipo particular de variable

- **¿Qué elementos definen a un objeto?**

	- La forma en que establecen comunicación e intercambia mensajes
	- Sus atributos y sus métodos ✔
	- Su cardinalidad y su tipo

- **¿Qué es herencia?**

  - Es a compilación de varios objetos
  - Es la dependencia de un estado
  - Es un mecanismo que permite basar una clase nueva en la definición de una clase existente ✔
  - Es una abstracción de un objeto

- **¿Qué significa instanciar una clase?**

	- Duplicar una clase
	- Conectar dos clases entre sí
	- Crear un objeto a partir de la clase ✔
	- Eliminar una clase

- **Son tipos de datos en Python:**

	- integer, str, float
	- int, varchar, list
	- int, str, float ✔
	- Ninguna es correcta

- **Los tipos de datos en Python se pueden clasificar en mutables e inmutables**

	- Verdadero ✔
	- Falso

- **Python viene preinstalado en el sistema operativo Windows**

	- Verdadero
	- Falso ✔

- **Si instalamos Python, ya podemos ejecutar instrucciones sin instalar software adicional**

	- Verdadero ✔
	- Falso

- **Python no admite tipos de datos compuestos, sólo de elementos individuales**

	- Verdadero
	- Falso ✔

---

## **Python orientado a datos I**

- **¿Cuál es la salida de este código?**

```py
x = np.arange(3, 9)
z = x.reshape(2, 3)
print(z[1][1])
```

- Seleccione una:

  - 8
  - 9
  - 6
  - 7 ✔

- **Para obtener los valores de x mayores a 37**

  - print(x > np.mean(x))
  - print(np[x > 37])
  - Ninguna es correcta
  - print(np.array[x > 37])
  - print(x[x > 37]) ✔

- **Para obtener los valores de x mayores al promedio**

  - print(x[x > np.mean(x)]) ✔
  - Ninguna es correcta
  - print(x > np.mean(x))
  - print(x > mean(x))

- **¿Cuál es la salida de este código?**

```py
x = np.arange(1, 5)
x = x*2
print(x[:3].sum())
```

- Seleccione una:

  - 12 ✔
  - 6
  - 8
  - 20

- **Para operar con dos arrays en numpy, éstos deben tener la misma estructura**

  - Verdadero
  - Falso ✔

---

## **Python orientado a datos II**

- **Tengo un set de datos con el ranking de popularidad de una serie de temas musicales. Para obtener un dataframe a partir de los datos del archivo ranking.csv:**

  - read_csv('ranking.csv')
  - pd.read_csv('ranking.csv')
  - df = pd.read_csv('ranking.csv') ✔
  - Ninguna es correcta

- **Para obtener las filas de datos que están por debajo del puesto 100:**

  - df[df['puesto'] > 100] ✔
  - df['puesto'] > 100
  - df[df['puesto'] < 100]
  - Ninguna es correcta

- **Para obtener las primeras 15 filas:**

  - df.first(15)
  - df.head(15) ✔
  - df.top(15)
  - Ninguna es correcta

- **Para obtener las últimas 5 filas:**

  - df.bottom(5)
  - df.bottom()
  - df.tail(5)
  - df.tail() ✔
  - Ninguna es correcta

- **¿Cuál es la salida del siguiente código?**

```py
data = {'a': [1, 2, 3}, 'b': {5, 8, 4]}
df = pd.DataFrame(data)
df{'c'] = df['a']+df['b']
print(df.iloc[2]['c'])
```

- Seleccione una:

  - 5
  - 10
  - 6
  - 7 ✔

- **No es posible utilizar las funciones de numpy y pandas al mismo tiempo**

  - Verdadero
  - Falso ✔

---

## **Python orientado a datos III**

- **¿Qué muestra un histograma?**

  - Desviaciones standard del promedio
  - Distribuciones de frecuencia ✔
  - Los valores medios
  - Ninguna respuesta es correcta

- **Para crear un gráfico de barras con una etiqueta horizontal 'Año' y etiqueta vertical 'Alumnos'**

  - ```py
    df.plot(type = 'bar')
    plt.xlabel=('Año')
    plt.ylabel=('Alumnos')
    ```
  - ```py
    df.plot(kind='bar' , xlabel = 'Año' , ylabel = 'Alumnos')
    ```
  - ```py
    df.plot(kind='bars' , xlabel = 'Año' , ylabel = 'Alumnos')
    ```
  - ```py
    df.plot(kind= 'bar')
    plt.xlabel=('Año')
    plt.ylabel=('Alumnos') ✔
    ```

- **¿Qué se obtiene del siguiente código?**

```py
df=pd.DataFrame({'deporte':['tenis', 'futbol', 'box', 'baseball'],'jugadores':[2, 22, 2, 18]})
df.groupby('deporte')['jugadores'].sum().plot(kind='pie')
```

- Seleccione una:

  - Un pie chart de 44 sectores
  - Un gráfico de 4 barras
  - Error
  - Un pie chart de 4 sectores ✔

- **En un gráfico de cajas, ¿qué representan los círculos fuera de la caja?**

  - Valores anómalos ✔
  - Valores promedio
  - Valores vacíos
  - Valores perdidos

- **df es un DataFrame que contiene la edad y género de un grupo de personas. ¿Cómo creamos un gráfico de barras horizontales que muestre la edad promedio de cada género?**

  - ```py
    df.groupby('genero')['edad'].average().plot(kind='barh', title='Edad promedio')
    ```
  - ```py
    df.groupby('genero')['edad'].mean().plot(kind='hbar', title='Edad promedio')
    ```
  - ```py
    df.groupby('genero')['edad'].average().plot(kind='hbar', title='Edad promedio')
    ```
  - ```py
    df.groupby('genero')['edad'].mean().plot(kind='barh', title='Edad promedio') ✔
    ```
