# **Big.Data**

## **Guia de estudio**

1. [Guía del estudiante](/1.%20Guia%20de%20estudio/1.%20Gu%C3%ADa%20del%20estudiante.pdf "Guia")
2. [Plan de estudio](/1.%20Guia%20de%20estudio/2.%20Plan%20de%20estudio.pdf "Plan")
3. [Info y material de las clases](/1.%20Guia%20de%20estudio/3.%20Info%20y%20material%20de%20las%20clases.pdf "Material")

---

## **Clases**

1. [Intro a Big Data](/2.%20Clases/1%20-%20Intro%20a%20Big%20Data.pdf "Intro")
2. [¿Que son las TIC?](/2.%20Clases/2.2%20-%20Que%20son%20las%20TIC.pdf "¿Que son las TIC?")
3. [Big Data](/2.%20Clases/3.3%20-%20Big%20Data.pdf "Big Data")
4. [Datos y Variables](/2.%20Clases/4.3%20-%20Datos%20y%20Variables.pdf "Datos y Variables")
5. [Algoritmos](/2.%20Clases/5.1%20-%20Algoritmos.pdf "Algoritmos")
6. [Bases de Datos I](/2.%20Clases/9.1%20-%20Bases%20de%20Datos%20I.pdf "Bases de Datos I")
7. [Bases de Datos II](/2.%20Clases/12.1%20-%20Bases%20de%20Datos%20II.pdf "Bases de Datos II")
8. [Cláusulas JOIN SQL](/2.%20Clases/Módulo%20I/12.2%20-%20Cláusulas%20JOIN%20SQL.pdf "Cláusulas JOIN SQL")
9. [Cláusulas Join SQL con ejemplos](/2.%20Clases/Módulo%20I/12.3%20-%20Cláusulas%20Join%20SQL%20con%20ejemplos.pdf "Cláusulas Join SQL con ejemplos")
10. [Planillas de cálculo - Operación Básica](/2.%20Clases/Módulo%20I/Documentación%20Aula%20Virtual/2%20-%20Planilla%20de%20cálculos/1%20-%20Operación%20básica/1%20-%20Planillas%20de%20cálculo%20-%20Operación%20Básica.pdf "Planillas de cálculo - Operación Básica")
11. [Planillas de cálculo - Fórmulas Básicas](/2.%20Clases/Módulo%20I/Documentación%20Aula%20Virtual/2%20-%20Planilla%20de%20cálculos/2%20-%20Fórmulas%20básicas/1%20-%20Planillas%20de%20cálculo%20-%20Fórmulas%20Básicas.pdf "Planillas de cálculo - Fórmulas Básicas")
12. [Planillas de cálculo U1 y U2](/2.%20Clases/Módulo%20I/Documentación%20Aula%20Virtual/2%20-%20Planilla%20de%20cálculos/4%20-%20Análisis%20y%20visualización/1%20-%20Planillas%20de%20cálculo%20U1%20y%20U2.pdf "Planillas de cálculo U1 y U2")
13. [Métricas en Marketing Digital](/2.%20Clases/Módulo%20I/Documentación%20Aula%20Virtual/3%20-%20Looker%20Studio/Unidad%20I/1%20-%20Métricas%20en%20Marketing%20Digital.pdf "Métricas en Marketing Digital")
14. [Looker Studio I](/2.%20Clases/Módulo%20I/Documentación%20Aula%20Virtual/3%20-%20Looker%20Studio/Unidad%20I/2%20-%20Looker%20Studio%20I.pdf "Looker Studio I")
15. [Looker Studio II](/2.%20Clases/Módulo%20I/Documentación%20Aula%20Virtual/3%20-%20Looker%20Studio/Unidad%20II/1%20-%20Looker%20Studio%20II.pdf "Looker Studio II")
16. [Primeros pasos en Python I](/2.%20Clases/Módulo%20II/Documentación%20Aula%20Virtual/1%20-%20Bases%20programación%20en%20Python/Unidades%201%20a%203.pdf "Primeros pasos en Python I")
17. [Primeros pasos en Python II](/2.%20Clases/Módulo%20II/Documentación%20Aula%20Virtual/1%20-%20Bases%20programación%20en%20Python/Unidades%204%20y%205.pdf "Primeros pasos en Python II")
18. [POO en Python](/2.%20Clases/Módulo%20II/Documentación%20Aula%20Virtual/2%20-%20POO%20en%20Python/Introducción%20a%20POO.pdf "POO en Python")
19. [BBDD en Python](/2.%20Clases/Módulo%20II/Documentación%20Aula%20Virtual/3%20-%20BBDD%20y%20GUI%20en%20Python/6%20-%20BBDD%20en%20Python.pdf "BBDD en Python")
20. [Estadistica I](/2.%20Clases/Módulo%20II/Documentación%20Aula%20Virtual/4%20-%20Estadistica/1%20-%20Unidad%201.pdf "Estadistica I")
21. [Estadistica II](/2.%20Clases/Módulo%20II/Documentación%20Aula%20Virtual/4%20-%20Estadistica/2%20-%20Unidad%202.pdf "Estadistica II")
22. [Numpy](/2.%20Clases/Módulo%20II/Documentación%20Aula%20Virtual/5%20-%20Python%20para%20análisis%20de%20datos/1%20-%20NumPy.pdf "Numpy")
23. [Pandas](/2.%20Clases/Módulo%20II/Documentación%20Aula%20Virtual/5%20-%20Python%20para%20análisis%20de%20datos/2.1%20-%20Pandas.pdf "Pandas")
24. [Matplotlib y Seaborn](/2.%20Clases/Módulo%20II/Documentación%20Aula%20Virtual/5%20-%20Python%20para%20análisis%20de%20datos/3%20-%20Matplotlib%20y%20Seaborn.pdf "Matplotlib y Seaborn")

### **Material de lectura Deepnote**

1. Estadistica

   - [Estadistica](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/1%20-%20Estadistica/1.%20Estadistica.pdf "Estadistica")
   - [Tendencia Central](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/1%20-%20Estadistica/2.%20Tendencia%20Central.pdf "Tendencia Central")
   - [Dispersión](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/1%20-%20Estadistica/3.%20Dispersión.pdf "Dispersión")
   - [Exploración visual](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/1%20-%20Estadistica/4.%20Exploración%20visual.pdf "Exploración visual")
   - [Correlaciones](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/1%20-%20Estadistica/5.%20Correlaciones.pdf "Correlaciones")

2. NumPy

   - [Numpy (Numerical Python)](</2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/2%20-%20Numpy/Numpy%20(Numerical%20Python).pdf.pdf> "Numpy (Numerical Python)")

3. Pandas

   - [Pandas](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/3%20-%20Pandas/1.%20Pandas.pdf "Pandas")
   - [Práctica Pandas](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/3%20-%20Pandas/2.%20Práctica%20Pandas.pdf "Práctica Pandas")
   - [Diccionarios y JSON](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/3%20-%20Pandas/3.%20Diccionarios%20y%20JSON.pdf "Diccionarios y JSON")

4. Matplotlib

   - [Introducción](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/4%20-%20Matplotlib/1.%20Introducción.pdf "Introducción")
   - [Histogramas](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/4%20-%20Matplotlib/2.%20Histogramas.pdf "Histogramas")
   - [Subplot](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/4%20-%20Matplotlib/3.%20Subplot.pdf "Subplot")
   - [Método orientado a objetos](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/4.%20Matplotlib/4%20-%20Método%20orientado%20a%20objetos.pdf "Método orientado a objetos")
   - [Contexto y estilo](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/4%20-%20Matplotlib/5.%20Contexto%20y%20estilo.pdf "Contexto y estilo")
   - [Bar Plot](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/4%20-%20Matplotlib/6.%20Bar%20Plot.pdf "Bar Plot")

5. Seaborn - repaso y extras

   - [Introducción](/2.%20Clases/Módulo%20II/Material%20de%20lectura%20Deepnote/5%20-%20Seaborn%20-%20repaso%20y%20extras/1.%20Introducción.pdf "Introducción")

---

## **Ejercicios Obligatorios**

1. [Descubriendo la plataforma](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20I/1%20-%20Descubriendo%20la%20plataforma.png "Descubriendo la plataforma")
2. [Conceptos generales](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20I/2%20-%20Conceptos%20generales.png "Conceptos generales")
3. [Algoritmos](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20I/3%20-%20Algoritmos.png "Algoritmos")
4. [Bases de datos I](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20I/4%20-%20Base%20de%20datos%20I.png "Bases de datos I")
5. [Bases de datos II](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20I/5%20-%20Base%20de%20datos%20II.png "Bases de datos II")
6. [Business Intelligence](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20I/6%20-%20Business%20Intelligence.png "Business Intelligence")
7. [Planilla de cálculos](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20I/7%20-%20Planilla%20de%20c%C3%A1lculos.png "Planilla de cálculos")
8. [Looker Studio](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20I/8%20-%20Looker%20Studio.png "Looker Studio")
9. [Bases de programación en Python I](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20II/1%20-%20Bases%20de%20programaci%C3%B3n%20en%20Python%20I.png "Bases de programación en Python I")
10. [Bases de programación en Python II](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20II/2%20-%20Bases%20de%20programaci%C3%B3n%20en%20Python%20II.png "Bases de programación en Python II")
11. [POO en Python](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20II/3%20-%20POO%20en%20Python.png "POO en Python")
12. [BBDD en Python I](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20II/4%20-%20BBDD%20en%20Python%20I.png "BBDD en Python I")
13. [BBDD en Python II](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20II/5%20-%20BBDD%20en%20Python%20II.png "BBDD en Python II")
14. [Estadistica](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20II/6%20-%20Estadistica.png "Estadistica")
15. [Ejercicios complementarios](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20II/7%20-%20Ejercicio%20complementario.png "Ejercicios complementarios")
16. [Python orientado a datos I](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20II/8%20-%20Python%20orientado%20a%20datos%20I.png "Python orientado a datos I")
17. [Python orientado a datos II](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20II/9%20-%20Python%20orientado%20a%20datos%20II.png "Python orientado a datos II")
18. [Python orientado a datos III](/3.%20Ejercicios%20Obligatorios/M%C3%B3dulo%20II/10%20-%20Python%20orientado%20a%20datos%20III.png "Python orientado a datos III")

---

## **Trabajo Practico**

- [Consigna TPN1](/4.%20Trabajo%20Practico/Módulo%20I/Consigna%20TPN1.pdf "Consigna TPN1")
- [Google Sheet TPN1](https://docs.google.com/spreadsheets/d/1B5-SPxiRrGrNeT_GlHVaSIFDPtc75SUY71UPMCEbepQ "Google Sheet TPN1")
- [Informe Looker Studio TPN1](https://lookerstudio.google.com/reporting/98645b18-2fca-4307-9351-67868a93eb5f "Informe Looker Studio TPN1")
- [Resultados TPN1](/4.%20Trabajo%20Practico/Módulo%20I/Resultados%20TPN1.pdf "Resultados TPN1")

---

## **Examenes Integradores**

1. [Primer examen](/5.%20Examenes%20Integradores/Primer%20examen.png "Primer examen")
2. [Segundo examen](/5.%20Examenes%20Integradores/Segundo%20examen.png "Segundo examen")
