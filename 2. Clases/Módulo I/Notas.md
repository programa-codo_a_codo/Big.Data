# **Clases**

## **1. Info a Big Data**

**Ejemplo para diferenciar dato de informacion**

```txt
Dato: Podria ser una fecha de nacimiento de una persona.
Informacion: Cuantos años tiene esa persona, que sale de calcular la diferencia entre la fecha de nacimiento y la fecha actual.
```

**Software, recursos y herramientas**

- [Mackaroo](https://www.mockaroo.com/ "Mackaroo") nos ayuda a generar automaticamente datos para una base de datos.

**Ciclo de vida del dato en la toma de decisiones**

- **Ingesta de datos**: Introduccion de datos.
- **Ingeniaria de datos**: Homogeniazar los datos, ordenarlos o adecuarlos.
- **Ciencia de datos**: analisis, agrupacion, filtro, calculo.
- **Explotacion de datos**: visualizacion de dotos, para quien tenga que tomar las decisiones, lo haga de forma adecuada.

## **2. Conceptos TIC**

Conceptos generales y TIC

La relación cliente / servidor. Front End / Back End. Cómo funciona Internet. Tareas que desarrolla un programador.
Lógica

Fundamentos de lógica para programadores.

No estructurado: Ej. AI que separa la voz de la banda sonora.

## **3. Big Data**

**Una buena seguridad debe ser: algo que saber (ej contraseña), posee (ej. tarjeta) y algo que uno es (ej. huella dactilar).**

El ing de datos hace ETL : edita, transforma y carga datos
